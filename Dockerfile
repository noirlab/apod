FROM python:3.9.7-slim-buster

WORKDIR /usr/src/app

COPY src/ /usr/src/app/

RUN pip install -r requirements.txt

CMD ["/bin/sh", "/usr/src/app/apod.sh"]
