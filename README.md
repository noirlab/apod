# Purpose of project

The purpose of this program is fetch the JSON data from year round from the website              
https://apod.nasa.gov/apod/astropix.html? starting from today's date.  This program is ran using 
python 3 and later versions.  If you need any earlier versions of python, be sure to import the  
correct libraries and modules.  Some of the methods from earlier modules might have to be changed
or renamed as well.  After the JSON data is collected, it will write it all to a file called     
jsonData.txt.  If you find any bugs or need help, feel free to email me.  There is currently one 
bug in this code.  Making large request from one year to today will cause a 503 error, which is  
a service unavailable.  This means the request is to large for the server side to handle.        
 
# Current project maintainers
Jason Kalawe jason.kalawe@noirlab.edu

# Dependencies
- [Data2Dome Website](http://data2dome.org)
- [NASA APOD Feed](https://api.nasa.gov/planetary/apod?api_key=udVoSficZZh0DkxGvylgywR99Wf3eNQMKT8DZSQW&)
- Python 3. PIP managed with requiurements.txt file.

# Deployment URL
- [hlcwebapps-lv1.norilab.edu] (http://feeds.data2dome.noirlab.edu/)

# Deployment filesystem location
- The project is symlinked: `/home/webapp/apps/apod/artifacts` -> `/var/www/html/apod`

# Production details

The Docker image is run daily using cron using the `webapp` user account.

# Runnng the project using Docker 

1. Create an `artifacts` directory with the contents of the `artifacts` directory in this repo.
2. Run the following command in the parent directory of `artifacts`: 

```sh
docker run -it --rm --name apod -v "$PWD/artifacts":/usr/src/app/artifacts registry.gitlab.com/noirlab/apod:latest
```
3. The container image will update the JSON files and LOG file in the `artifacts` directory.

# Docker development

```sh
docker run -it --rm --name apod -v "$PWD":/usr/src/app -w /usr/src/app python:alpine3.14 sh
```

# Original project maintainers
Christian Soto cjsoto@lsst.org

# Original Deployment URL
- [Jelastic] (http://epo-d2d.ric.jelastic.vps-host.net)
- https://feeds.data2dome.org/apod_v2.json

# Original Deployment filesystem location
- jelastic.com:/ROOT
